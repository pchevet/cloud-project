# Cloud Project 2024

Projet avec :
- en front-end
    + un serveur CTFd pouvant héberger des challenges de cybersécurité
    + cyberchef sur la route /cyberchef, un outil couteau suisse très utile pour les challenges
    
- en back-end :
    + sur la route /challenge1
        - un serveur apache avec un site PHP se connectant à une base de données sur un autre serveur 
            (avec un fichier robots.txt pour trouver fichier bdd.php avec injection sql possible)
        - route /challenge1/phpmyadmin pour accéder au phpmyadmin (en exploitant les identifiants trouvés injection SQL par exemple)
        - stockage fichier possible sur stockage séparé avec /files.php (envoi de fichier en POST)
    + un serveur nginx portant un simple site HTML/CSS/JS
    + un serveur nextcloud pour utiliser le système de fichier sur la machine
- sur le serveur bdd
    + une base de donnée liée au challenge1