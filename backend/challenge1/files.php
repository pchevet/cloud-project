<?php
$directory = '/storage';

if (is_dir($directory)) {
    if ($dh = opendir($directory)) {
        while (($file = readdir($dh)) !== false) {
            echo "filename: $file<br>";
        }
        closedir($dh);
    } else {
        echo "Unable to open directory.";
    }
} else {
    echo "Directory does not exist.";
}

// Permettre de charger des fichiers en POST sur /mnt
if (isset($_FILES['file'])) {
    $targetPath = $directory . '/' . basename($_FILES['file']['name']);
    if (move_uploaded_file($_FILES['file']['tmp_name'], $targetPath)) {
        echo "Fichier envoyé avec succès.";
    } else {
        echo "Erreur lors de l'envoi du fichier.";
    }
}

// permettre de télécharger des fichiers
if (isset($_GET['file'])) {
    $file = $directory . '/' . $_GET['file'];
    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
    } else {
        echo "Le fichier n'existe pas.";
    }
}
?>