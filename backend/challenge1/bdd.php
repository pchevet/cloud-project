<?php
// Connexion à la base de données
$mysqli = new mysqli("192.168.120.4", "root", "root", "my_database");

// Vérifiez la connexion
if ($mysqli->connect_error) {
    die("Erreur de connexion : " . $mysqli->connect_error);
}

// Créez une table minimale
$tableCreationQuery = "
    CREATE TABLE IF NOT EXISTS users (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(100) NOT NULL
    )
";
if (!$mysqli->query($tableCreationQuery)) {
    die("Erreur de création de la table : " . $mysqli->error);
}

// Insérez des données de test
$insertQuery = "
    INSERT INTO users (name)
    VALUES ('Alice'), ('Bob'), ('Charlie')
";
$mysqli->query($insertQuery);

// Permettre une injection SQL
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $query = "SELECT * FROM users WHERE id = $id"; // Vulnérable à l'injection SQL
    $result = $mysqli->query($query);

    if ($result) {
        while ($row = $result->fetch_assoc()) {
            print_r($row);
        }
        $result->free();
    } else {
        echo "Erreur dans la requête : " . $mysqli->error;
    }
} else {
    echo "Veuillez fournir un paramètre 'id' dans l'URL, par exemple ?id=1";
}

// Fermez la connexion
$mysqli->close();
?>